/* Name: katlego Moikabi
 * Module : 2 
 * Question 1
 * Purpose : Stores &amp; print name, favorite app, and city
 */

void main() {
  //Declare Array
  var pastWinners = [];  
  
  //2021
  pastWinners.add("Ambani Africa");
  //2020
  pastWinners.add("EasyEquities");
  //2019
  pastWinners.add("Naked Insurance");
  //2018
  pastWinners.add("Khula ecosystem ");
  //2017
  pastWinners.add("Shyft");
  //2016
  pastWinners.add("Domestly");
  //2015
  pastWinners.add("WumDrop");
  //2014
  pastWinners.add("Pineapple");
  //2013
  pastWinners.add("SnapScan");
  //2012
  pastWinners.add("FNB Banking");
  pastWinners.sort();

  //Display/output data
 print("a) Past Winners from 2012-2021 \n ----------------------");
  
 for(int i = 0; i<pastWinners.length; i++){
   print("${i+1}. ${pastWinners[i]}");
}
  
print("\n b) 2017 Winner Is : ${pastWinners[7]} \n 2018 Winner Is : ${pastWinners[4]}\n ---------------------- \n c) Total Number Of Apps = ${pastWinners.length}");

}