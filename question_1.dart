/* Name: katlego Moikabi
 * Module : 2 
 * Question 1
 * Purpose :stores &amp; print name, favorite app, and city
 */

void main() {
  //Declare Variables
 String name, favoriteApp, city;
  
  //Initialize Variables
  name = 'Katlego Moikabi';
  favoriteApp = "Nedbank App";
  city = "Gauteng";
  
  //Display/output data
  print ('I am $name from $city , my favourite App is $favoriteApp');
}